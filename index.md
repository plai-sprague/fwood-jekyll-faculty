---
layout: default
---

## About Me

<img class="profile-picture" src="headshot.jpg">

I am an associate professor of [computer science](http://www.cs.ubc.ca/) at the [University of British Columbia](http://www.ubc.ca/) and a [Canada CIFAR AI Chair](https://www.cifar.ca/ai/pan-canadian-artificial-intelligence-strategy) at [Mila](https://mila.quebec/).

[fwood@cs.ubc.ca](mailto:fwood@cs.ubc.ca)  
<a href="https://twitter.com/frankdonaldwood" class="twitter-follow-button" data-show-count="false">Follow @frankdonaldwood</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## Research Interests

My primary research areas are probabilistic programming and applied probabilistic
machine learning. My research interests range from the development of new
probabilistic models and inference algorithms to real-world applications. My
research contributions include probabilistic programming systems, new models and
inference algorithms, and novel applications of such models to problems in
computational neuroscience, vision, natural language processing, robotics, and reinforcement learning..

## Selected Publications

{% bibliography --file selected.bib %}

### Prospective students
I am always looking for excellent, academically-motivated, AI-inspired PhD students.  Please visit [the departmental web page for prospective students](https://www.cs.ubc.ca/students/grad/prospective) to apply.  An excellent strategy for getting an offer to work with me is to propose research in your proposal that extends and explicitly cites recent work of my own.  Better still, working with me beforehand is a major leg up.  Direct email to me about becoming a student at UBC is _extremely unlikely_ to get a reply.

### Postdocs
I am constantly looking for postdocs with strong programming languages, statistics, and applied machine learning skills.  Please [contact me directly](mailto:fwood@cs.ubc.ca) including a brief (one paragraph or so) proposed research plan related to my recent research and funding (see my [CV](cv.pdf) for current funding details).  Contact without research plans is _extremely unlikely_ to get a reply.

### Entrepreneurs
The intersection of Canada, British Columbia, Vancouver, and UBC is an amazingly interesting place to start a technology company. [Contact me directly](mailto:fwood@cs.ubc.ca) if you are interested in maintaining an academic affiliation while starting, while sitting beside a world-class machine learning team, a machine learning startup in Vancouver.  Please familiarize yourself with [MITACS Accelerate](https://www.mitacs.ca/en/programs/accelerate) and explain where you will source initial investment in the first email.
Contact without concrete and attainable funding plans is _extremely unlikely_ to get a reply.

### Highlights
I was one of the initial developers of the [Anglican probabilistic programming language](https://probprog.github.io/anglican/index.html) and am a co-author of one of the first textbooks about probabilistic programming, a pre-print of which is [on arXiv](https://arxiv.org/abs/1809.10756).
